package com.crazymakercircle.designpattern.strategy;

//抽象策略类
public interface Strategy {
    void show();
}