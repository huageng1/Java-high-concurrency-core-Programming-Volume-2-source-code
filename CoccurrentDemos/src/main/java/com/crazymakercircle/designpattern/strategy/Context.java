package com.crazymakercircle.designpattern.strategy;

import lombok.Data;

@Data
public class Context {

    String type = System.getenv("strategy");
    Strategy strategy = null;

    public Context() {
        switch (type) {
            case "1":
                strategy = new StrategyA();
                break;
            case "2":
                strategy = new StrategyB();
                break;
            case "3":
                strategy = new StrategyC();
                break;
            default:
                strategy = new StrategyA();


        }

    }
}
