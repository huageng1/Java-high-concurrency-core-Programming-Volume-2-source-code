package com.crazymakercircle.httpclient.protocol;


import com.crazymakercircle.httpclient.common.TypeReference;

public class ReqOptions {
    public ReqOptions(TypeReference type) {
        this.type = type;
    }

    private TypeReference type;

    public TypeReference getType() {
        return type;
    }
}
