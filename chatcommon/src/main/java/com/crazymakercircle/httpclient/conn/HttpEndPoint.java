package com.crazymakercircle.httpclient.conn;


import com.crazymakercircle.httpclient.common.EndPoint;
import com.crazymakercircle.httpclient.common.RpcException;

public interface HttpEndPoint extends EndPoint {

    void send(HttpRequest request) throws RpcException;

}
