package com.crazymakercircle.threadpool;

import com.crazymakercircle.util.Print;

public class RunnableWrapper implements Runnable{
    private final Integer taskId;

    public RunnableWrapper(Integer taskId) {
        this.taskId = taskId;
    }

    public Integer getTaskId() {
        return this.taskId;
    }

    @Override
    public void run() {
        Print.tcfo("Task " + taskId + " is running.");
        try {
            Thread.sleep(100);
        } catch (Exception e) {
            e.printStackTrace();
            // ignore
        }
        Print.tcfo("Task " + taskId + " is completed.");
    }
}
